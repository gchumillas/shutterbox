import React from 'react'
import { useDispatch } from 'react-redux'
import { StyleSheet, View } from 'react-native'
import { Text } from 'react-native-elements'
import { useTranslation } from 'react-i18next'
import { toggleMenu } from '../../store/slices/isMenuOpen'
import IconButton from '../../components/IconButton'

const Header = () => {
  const { t } = useTranslation()
  const dispatch = useDispatch()

  return (
    <View style={styles.root}>
      <IconButton name="menu" onPress={() => dispatch(toggleMenu())} />
      <Text style={styles.title}>{t('header.title')}</Text>
      <IconButton name="file-upload" />
    </View>
  )
}

const styles = StyleSheet.create({
  root: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%'
  },
  title: {
    flex: 1,
    textAlign: 'center',
    fontSize: 18
  }
})

export default Header
