import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, View } from 'react-native'
import Header from './Header'

const DefaultLayout = ({ children }) => {
  return (
    <View>
      <Header />
      <View style={styles.root}>{children}</View>
    </View>
  )
}

DefaultLayout.propTypes = {
  children: PropTypes.node
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
})

export default DefaultLayout
