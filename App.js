import { StatusBar } from 'expo-status-bar'
import React from 'react'
import { Provider, useSelector, useDispatch } from 'react-redux'
import { getStatusBarHeight } from 'react-native-status-bar-height'
import { StyleSheet, View } from 'react-native'
import { NativeRouter, Switch, Route, Link } from 'react-router-native'
import { ListItem, Icon } from 'react-native-elements'
import { useTranslation } from 'react-i18next'
import DashboardPage from './pages/DashboardPage'
import UploadImagesPage from './pages/UploadImagesPage'
import SearchImagesPage from './pages/SearchImagesPage'
import MapPage from './pages/MapPage'
import TagsPage from './pages/TagsPage'
import TrashPage from './pages/TrashPage'
import LogoutPage from './pages/LogoutPage'
import store from './store'
import { toggleMenu } from './store/slices/isMenuOpen'
import DrawerLayout from './components/DrawerLayout'
import './locales'
import './firebase'

const App = () => {
  const { t } = useTranslation()
  const dispatch = useDispatch()
  const isMenuOpen = useSelector(state => state.isMenuOpen.value)
  const menuItems = [
    {
      icon: 'home',
      title: t('menu.dashboard'),
      url: '/'
    },
    {
      icon: 'file-upload',
      title: t('menu.uploadImages'),
      url: '/upload-images'
    },
    {
      icon: 'search',
      title: t('menu.searchImages'),
      url: '/search-images'
    },
    {
      icon: 'map',
      title: t('menu.map'),
      url: '/map'
    },
    {
      icon: 'loyalty',
      title: t('menu.tags'),
      url: '/tags'
    },
    {
      icon: 'delete',
      title: t('menu.trash'),
      url: '/trash'
    },
    {
      icon: 'power-settings-new',
      title: t('menu.logout'),
      url: '/logout'
    }
  ]

  const navigation = (
    <View style={styles.drawer}>
      {menuItems.map((item, i) => (
        <Link key={i} to={item.url} onPress={() => dispatch(toggleMenu())}>
          <ListItem bottomDivider>
            <Icon name={item.icon} />
            <ListItem.Content>
              <ListItem.Title>{item.title}</ListItem.Title>
            </ListItem.Content>
          </ListItem>
        </Link>
      ))}
    </View>
  )

  return (
    <NativeRouter>
      <DrawerLayout
        open={isMenuOpen}
        onToggle={open => dispatch(toggleMenu(open))}
        navigation={navigation}
      >
        <View style={styles.root}>
          <Switch>
            <Route exact path="/" component={DashboardPage} />
            <Route exact path="/upload-images" component={UploadImagesPage} />
            <Route exact path="/search-images" component={SearchImagesPage} />
            <Route exact path="/map" component={MapPage} />
            <Route exact path="/tags" component={TagsPage} />
            <Route exact path="/trash" component={TrashPage} />
            <Route exact path="/logout" component={LogoutPage} />
          </Switch>
          <StatusBar style="auto" />
        </View>
      </DrawerLayout>
    </NativeRouter>
  )
}

const StoreWrapper = () => {
  return (
    <Provider store={store}>
      <App />
    </Provider>
  )
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    alignItems: 'center',
    paddingTop: getStatusBarHeight()
  },
  drawer: {
    flex: 1,
    paddingTop: getStatusBarHeight(),
    padding: 8
  }
})

export default StoreWrapper
