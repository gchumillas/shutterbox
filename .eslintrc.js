// eslint-disable-next-line
module.exports = {
  settings: {
    react: {
      version: 'detect'
    }
  },
  env: {
    browser: true,
    es2021: true
  },
  extends: ['eslint:recommended', 'plugin:react/recommended'],
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: 12,
    sourceType: 'module'
  },
  plugins: ['react'],
  rules: {
    eqeqeq: 'off', // the server may return strings or numbers indistinctly
    camelcase: 'off', // the server may return snake_case variables
    'no-console': 'off', // we are mostly on the web
    'semi': ['error', 'never']
  }
}
