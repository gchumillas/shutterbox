import { configureStore } from '@reduxjs/toolkit'
import isMenuOpen from './slices/isMenuOpen'

export default configureStore({
  reducer: {
    isMenuOpen
  }
})
