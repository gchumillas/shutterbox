import { createSlice } from '@reduxjs/toolkit'

export const isMenuOpen = createSlice({
  name: 'isMenuOpen',
  initialState: {
    value: false
  },
  reducers: {
    toggleMenu: (state, { payload: isOpen = undefined }) => {
      state.value = isOpen ?? !state.value
    }
  }
})

export const { toggleMenu } = isMenuOpen.actions
export default isMenuOpen.reducer
