import React from 'react'
import PropTypes from 'prop-types'
import { DrawerLayoutAndroid } from 'react-native'

const DrawerLayout = ({ open, onToggle, navigation, children }) => {
  const drawerRef = React.useRef(null)

  React.useEffect(() => {
    const drawerElem = drawerRef.current

    open ? drawerElem?.openDrawer() : drawerElem?.closeDrawer()
  }, [open])

  // TODO: replace drawerWidth by a lower number
  return (
    <DrawerLayoutAndroid
      ref={drawerRef}
      drawerWidth={300}
      drawerPosition="left"
      renderNavigationView={() => navigation}
      onDrawerClose={() => onToggle(false)}
      onDrawerOpen={() => onToggle(true)}
    >
      {children}
    </DrawerLayoutAndroid>
  )
}

DrawerLayout.propTypes = {
  open: PropTypes.bool.isRequired,
  onToggle: PropTypes.func.isRequired, // (open: boolean) => void
  navigation: PropTypes.element.isRequired,
  children: PropTypes.node
}

export default DrawerLayout
