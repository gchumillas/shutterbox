import React from 'react'
import PropTypes from 'prop-types'
import { Pressable } from 'react-native'
import { Icon } from 'react-native-elements'

const IconButton = ({ size = 24, ...iconProps }) => {
  const padding = size / 2
  const buttonSize = size + padding * 2

  return (
    <Pressable
      android_ripple={{ radius: size }}
      style={[
        {
          borderRadius: buttonSize / 2,
          width: buttonSize,
          height: buttonSize,
          justifyContent: 'center',
          alignItems: 'center'
        }
      ]}
    >
      <Icon size={size} {...iconProps} />
    </Pressable>
  )
}

IconButton.propTypes = {
  size: PropTypes.number
}

export default IconButton
