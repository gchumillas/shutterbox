import React from 'react'
import { Text } from 'react-native-elements'
import { useTranslation } from 'react-i18next'
import DefaultLayout from '../layouts/DefaultLayout'

const TrashPage = () => {
  const { t } = useTranslation('trash')

  return (
    <DefaultLayout>
      <Text>{t('text')}</Text>
    </DefaultLayout>
  )
}

export default TrashPage
