import React from 'react'
import { Text } from 'react-native-elements'
import { useTranslation } from 'react-i18next'
import DefaultLayout from '../layouts/DefaultLayout'

const LogoutPage = () => {
  const { t } = useTranslation('logout')

  return (
    <DefaultLayout>
      <Text>{t('text')}</Text>
    </DefaultLayout>
  )
}

export default LogoutPage
