import React from 'react'
import { Text } from 'react-native-elements'
import { useTranslation } from 'react-i18next'
import DefaultLayout from '../layouts/DefaultLayout'

const MapPage = () => {
  const { t } = useTranslation('map')

  return (
    <DefaultLayout>
      <Text>{t('text')}</Text>
    </DefaultLayout>
  )
}

export default MapPage
