import common from './common'
import dashboard from './dashboard'
import uploadImages from './upload-images.json'
import searchImages from './search-images.json'
import map from './map.json'
import tags from './tags.json'
import trash from './trash.json'
import logout from './logout.json'

export default { common, dashboard, uploadImages, searchImages, map, tags, trash, logout }
