import * as Localization from 'expo-localization'
import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'
import en from './en'
import es from './es'

i18n.use(initReactI18next).init({
  lng: Localization.locale,
  fallbackLng: 'en',
  defaultNS: 'common',
  resources: { en, es },
  interpolation: {
    escapeValue: false
  },
  cleanCode: true
})

export default i18n
